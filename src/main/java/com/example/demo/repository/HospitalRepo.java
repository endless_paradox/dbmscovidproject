package com.example.demo.repository;

import com.example.demo.entity.Covid19Hospitals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HospitalRepo extends JpaRepository<Covid19Hospitals, Integer> {
	List<Covid19Hospitals> findByDistrict_Id(int id);
}
