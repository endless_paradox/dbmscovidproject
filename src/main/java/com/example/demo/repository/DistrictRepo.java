package com.example.demo.repository;

import com.example.demo.entity.Covid19District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRepo extends JpaRepository<Covid19District, Integer> {
	
	List<Covid19District> findByState_Id(int id);
	
}
