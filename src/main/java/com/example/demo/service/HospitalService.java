package com.example.demo.service;

import com.example.demo.entity.Covid19Hospitals;

import java.util.List;

public interface HospitalService {
	
	Covid19Hospitals getHospitals(int id);
	
	List<Covid19Hospitals> getAllHospitals();
	
	Covid19Hospitals addHospital(Covid19Hospitals covid19Hospitals);
	
	List<Covid19Hospitals> getHospitalsUsingDistrictId(int id);
	
}
