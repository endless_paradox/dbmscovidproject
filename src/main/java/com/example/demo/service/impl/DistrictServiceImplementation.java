package com.example.demo.service.impl;

import com.example.demo.entity.Covid19District;
import com.example.demo.repository.DistrictRepo;
import com.example.demo.service.DistrictService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DistrictServiceImplementation implements DistrictService {
	
	private final DistrictRepo districtRepo;
	
	@Override
	public Covid19District getDistrict(int id) {
		return districtRepo.getById(id);
	}
	
	@Override
	public List<Covid19District> getAllDistricts() {
		return districtRepo.findAll();
	}
	
	@Override
	public Covid19District addDistrict(Covid19District covid19District) {
		return districtRepo.save(covid19District);
	}
	
	@Override
	public List<Covid19District> getByStateId(int id) {
		return districtRepo.findByState_Id(id);
	}
}
