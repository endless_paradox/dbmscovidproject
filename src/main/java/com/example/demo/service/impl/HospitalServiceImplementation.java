package com.example.demo.service.impl;

import com.example.demo.entity.Covid19Hospitals;
import com.example.demo.repository.HospitalRepo;
import com.example.demo.service.HospitalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HospitalServiceImplementation implements HospitalService {
	
	private final HospitalRepo hospitalRepo;
	
	@Override
	public Covid19Hospitals getHospitals(int id) {
		return hospitalRepo.getById(id);
	}
	
	@Override
	public List<Covid19Hospitals> getAllHospitals() {
		return hospitalRepo.findAll();
	}
	
	@Override
	public Covid19Hospitals addHospital(Covid19Hospitals covid19Hospitals) {
		return hospitalRepo.save(covid19Hospitals);
	}
	
	@Override
	public List<Covid19Hospitals> getHospitalsUsingDistrictId(int id) {
		return hospitalRepo.findByDistrict_Id(id);
	}
}
