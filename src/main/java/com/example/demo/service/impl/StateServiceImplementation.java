package com.example.demo.service.impl;

import com.example.demo.entity.Covid19State;
import com.example.demo.repository.StateRepo;
import com.example.demo.service.StateInterface;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StateServiceImplementation implements StateInterface {
	
	private final StateRepo stateRepo;
	
	@Override
	public Covid19State getState(int id) {
		return stateRepo.getById(id);
	}
	
	@Override
	public List<Covid19State> getAllStates() {
		return stateRepo.findAll();
	}
	
	@Override
	public Covid19State addState(Covid19State covid19State) {
		return stateRepo.save(covid19State);
	}
}
