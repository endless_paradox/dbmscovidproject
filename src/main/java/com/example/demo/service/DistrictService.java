package com.example.demo.service;

import com.example.demo.entity.Covid19District;

import java.util.List;

public interface DistrictService {

	Covid19District getDistrict(int id);
	List<Covid19District> getAllDistricts();
	Covid19District addDistrict(Covid19District covid19District);
	List<Covid19District> getByStateId(int id);

}
