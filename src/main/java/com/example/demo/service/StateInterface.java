package com.example.demo.service;

import com.example.demo.entity.Covid19State;

import java.util.List;

public interface StateInterface {
	
	Covid19State getState(int id);
	
	List<Covid19State> getAllStates();
	
	Covid19State addState(Covid19State covid19State);
	
}
