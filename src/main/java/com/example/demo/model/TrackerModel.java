package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrackerModel {
	
	private long totalNoOfCases;
	private long totalNoOfActiveCases;
	private long recovered;
	private long dead;
}
