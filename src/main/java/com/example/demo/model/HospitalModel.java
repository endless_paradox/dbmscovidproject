package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalModel {
	private int id;
	private String name;
	private long noOfBeds;
	private String districtName;
}
