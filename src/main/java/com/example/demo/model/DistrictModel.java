package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistrictModel {
	private int id;
	private String name;
	private long totalNoOfCases;
	private long totalNoOfActiveCases;
	private long recovered;
	private long dead;
	private long noOfHospitals;
	private long firstDoseCompleted;
	private long secondDoseCompleted;
	private long totalNoOfBeds;
}
