package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Covid19District {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private long noOfPatients;
	private long noOfActivePatients;
	private long recovered;
	private long died;
	private long firstDose;
	private long secondDose;
	
	@OneToMany(mappedBy = "district")
	private List<Covid19Hospitals> hospitals;
	
	@ManyToOne
	@JoinColumn(name = "state_id", nullable = false)
	private Covid19State state;
	
}
