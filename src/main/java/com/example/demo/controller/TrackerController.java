package com.example.demo.controller;

import com.example.demo.entity.Covid19District;
import com.example.demo.entity.Covid19Hospitals;
import com.example.demo.entity.Covid19State;
import com.example.demo.model.DistrictModel;
import com.example.demo.model.HospitalModel;
import com.example.demo.model.StateModel;
import com.example.demo.model.TrackerModel;
import com.example.demo.repository.DistrictRepo;
import com.example.demo.service.DistrictService;
import com.example.demo.service.HospitalService;
import com.example.demo.service.StateInterface;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class TrackerController {
	
	private final StateInterface stateInterface;
	private final DistrictService districtService;
	private final HospitalService hospitalService;

	@RequestMapping(value = "/index")
	public String index(Model model)   {
		List<Covid19State> covid19StateList = stateInterface.getAllStates();
		TrackerModel trackerModel = new TrackerModel();
		trackerModel.setTotalNoOfCases(covid19StateList.stream().map(Covid19State::getDistricts)
				.map(e -> e.stream().map(Covid19District::getNoOfPatients)
				.reduce(0L, Math::addExact))
				.reduce(0L, Math::addExact));
		trackerModel.setTotalNoOfActiveCases(covid19StateList.stream().map(Covid19State::getDistricts)
				.map(e -> e.stream().map(Covid19District::getNoOfActivePatients)
						.reduce(0L, Math::addExact))
				.reduce(0L, Math::addExact));
		trackerModel.setRecovered(covid19StateList.stream().map(Covid19State::getDistricts)
				.map(e -> e.stream().map(Covid19District::getRecovered)
						.reduce(0L, Math::addExact))
				.reduce(0L, Math::addExact));
		trackerModel.setDead(covid19StateList.stream().map(Covid19State::getDistricts)
				.map(e -> e.stream().map(Covid19District::getDied)
						.reduce(0L, Math::addExact))
				.reduce(0L, Math::addExact));
		model.addAttribute("tracker", trackerModel);
		return "index";
	}
	
	@RequestMapping("/state")
	public String getStates(Model model)    {
		List<Covid19State> covid19StateList = stateInterface.getAllStates();
		List<StateModel> stateModels = new ArrayList<>();
		for(Covid19State covid19State: covid19StateList)    {
			StateModel stateModel = new StateModel();
			stateModel.setId(covid19State.getId());
			stateModel.setName(covid19State.getName());
			stateModel.setTotalNoOfCases(covid19State.getDistricts().stream().map(Covid19District::getNoOfPatients).reduce(0L, Math::addExact));
			stateModel.setTotalNoOfActiveCases(covid19State.getDistricts().stream().map(Covid19District::getNoOfActivePatients).reduce(0L, Math::addExact));
			stateModel.setNoOfDistricts(covid19State.getDistricts().size());
			stateModel.setRecovered(covid19State.getDistricts().stream().map(Covid19District::getRecovered).reduce(0L, Math::addExact));
			stateModel.setDead(covid19State.getDistricts().stream().map(Covid19District::getDied).reduce(0L, Math::addExact));
			stateModel.setFirstDoseCompleted(covid19State.getDistricts().stream().map(Covid19District::getFirstDose).reduce(0L, Math::addExact));
			stateModel.setSecondDoseCompleted(covid19State.getDistricts().stream().map(Covid19District::getSecondDose).reduce(0L, Math::addExact));
			stateModels.add(stateModel);
		}
		model.addAttribute("states", stateModels);
		return "state";
	}
	
	@RequestMapping(path = "/district/{id}")
	public String getDistricts(@PathVariable("id") int id, Model model)    {
		List<Covid19District> covid19Districts = districtService.getByStateId(id);
		List<DistrictModel> districtModels = new ArrayList<>();
		for(Covid19District covid19District: covid19Districts)    {
			DistrictModel districtModel = new DistrictModel();
			districtModel.setId(covid19District.getId());
			districtModel.setName(covid19District.getName());
			districtModel.setTotalNoOfCases(covid19District.getNoOfPatients());
			districtModel.setTotalNoOfActiveCases(covid19District.getNoOfActivePatients());
			districtModel.setNoOfHospitals(covid19District.getHospitals().size());
			districtModel.setRecovered(covid19District.getRecovered());
			districtModel.setDead(covid19District.getDied());
			districtModel.setFirstDoseCompleted(covid19District.getFirstDose());
			districtModel.setSecondDoseCompleted(covid19District.getSecondDose());
			districtModel.setTotalNoOfBeds(covid19District.getHospitals().stream().map(e -> e.getNoOfBeds()).reduce(0L, Math::addExact));
			districtModels.add(districtModel);
		}
		model.addAttribute("districts", districtModels);
		return "district";
	}
	
	@RequestMapping(path = "/hospital/{id}")
	public String getHospitals(@PathVariable("id") int id, Model model)    {
		List<Covid19Hospitals> covid19Hospitals = hospitalService.getHospitalsUsingDistrictId(id);
		List<HospitalModel> hospitalModels = new ArrayList<>();
		for(Covid19Hospitals covid19Hospitals1: covid19Hospitals)    {
			HospitalModel hospitalModel = new HospitalModel();
			hospitalModel.setName(covid19Hospitals1.getName());
			hospitalModel.setId(covid19Hospitals1.getId());
			hospitalModel.setNoOfBeds(covid19Hospitals1.getNoOfBeds());
			hospitalModel.setDistrictName(districtService.getDistrict(id).getName());
			hospitalModels.add(hospitalModel);
		}
		model.addAttribute("hospitals", hospitalModels);
		return "hospital";
	}
	
	
	

}
